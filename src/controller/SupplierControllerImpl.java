package controller;


import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import model.Supplier;
import model.SupplierImpl;
import model.VideogameCenter;
import view.ChoiceSupplierViewImpl;
import view.InsertSupplierViewImpl;
import view.MainPanelImpl;
import view.ModifySupplierViewImpl;
import view.SupplierViewImpl;

public class SupplierControllerImpl implements SupplierController {
	
	private VideogameCenter modelCenter;
	private MainPanelImpl mainPanel;
	private SupplierViewImpl supplierView;
	
	private InsertSupplierViewImpl insertSupplierView;
	private ModifySupplierViewImpl modifySupplierView;
	
	private ChoiceSupplierViewImpl choiceSupplierView; 
	private JTextField choiceTextField;  //<--- questo va usato nel pulsante modifica prima di quit
	private JFrame choiceFrame;
	
	public JTextField getChoiceTextField() {return choiceTextField; }
	
/** used in mainController.choiceSupplierView(JTextField choiceTextField ,JFrame choiceFrame)  */ //<--- questo method va implementato
	
	public SupplierControllerImpl(
			VideogameCenter modelCenter, 
			JTextField choiceTextField ,
			JFrame choiceFrame,
			ChoiceSupplierViewImpl choiceSupplierView 
			//---------------------------------------------> VEDI ANCHE quitChoice che va usato in ChoiceSupplierViewImpl
			) {

		this.modelCenter = modelCenter;
		this.choiceTextField = choiceTextField;
		this.choiceFrame = choiceFrame;
		this.choiceSupplierView = choiceSupplierView; //<--- implementata la classe decommenta

	}
	
	/** used in mainController.insertSupplierView() */
	
	public SupplierControllerImpl(
			VideogameCenter modelCenter, 
			MainPanelImpl mainPanel,
			InsertSupplierViewImpl insertSupplier) {

		this.modelCenter = modelCenter;
		this.mainPanel = mainPanel;
		this.insertSupplierView = insertSupplier;

	}
	/** used in mainController.Supplier() */
	public SupplierControllerImpl(
			VideogameCenter modelCenter, 
			MainPanelImpl mainPanel,
			SupplierViewImpl supplierView) {

		this.modelCenter = modelCenter;
		this.mainPanel = mainPanel;
		this.supplierView = supplierView;

	}
	
	/**used in SupplierController.setModifyController */
	public SupplierControllerImpl(VideogameCenter modelCenter,SupplierViewImpl supplierView,ModifySupplierViewImpl modify) {

		this.modelCenter = modelCenter;
		this.supplierView = supplierView;
		this.modifySupplierView = modify;

	}
	
	
	
	@Override
	public void insertSupplier(String name, int code) {
		Supplier newSupplier = new SupplierImpl(name, code);
		modelCenter.addSupplier(newSupplier);
		
	}

	@Override
	public VideogameCenter getCenter() {
		return this.modelCenter;
	}

	@Override
	public String modifySupplier(String name, int code, JLabel displayNameSupplier) {
		String check = null;

		 

			for (int i = 0; i < modelCenter.getListSupplier().size(); i++) {

				if (modelCenter.getListSupplier().get(i).getCodeSupplier() == code) {

					/*if (checkName(name) == true) {

						check = Utility.Utility.CHECKQUANTITY;

						modelCenter.getListSupplier().get(i).setMaxGameSupplier(max);
						directPanel.setPanel(this.modelCenter.getListSupplier());
							
						
						return check;

					} else */
					{

						modelCenter.getListSupplier().get(i).setName(name);

						supplierView.setPanel(this.modelCenter.getListSupplier());
						

						check = Utility.Utility.SUCCESSMODIFY;

					}
				}

			}
		


		return check;

	}

	@Override
	public boolean checkName(String name) {
		for (int i = 0; i < modelCenter.getListSupplier().size(); i++) {

			if (modelCenter.getListSupplier().get(i).getName().equals(name)) {

				return true;
			}

		}

		return false;
	}

	@Override
	public boolean checkCode(int code) {
		for (int i = 0; i < modelCenter.getListSupplier().size(); i++) {

			if (modelCenter.getListSupplier().get(i).getCodeSupplier() == code) {

				return true;

			}

		}

		return false;
	}

	
	@Override
	public void quit() {
		//io arrivo qui da 
		//MainController.Supplier (devo chiudere supplierView)
		// e devo visualizzare mainPanel
		
		this.mainPanel.setVisible(true);
		//if (this.insertSupplierView != null) this.insertSupplierView.setVisible(false);
		if (this.supplierView != null) this.supplierView.setVisible(false);
		
	}
	
	@Override
	public void quitInsert() {
		//io arrivo qui da 
		//MainController.insertSupplierView (devo chiudere insertSupplierView)
		// e devo visualizzare mainPanel
		
		this.mainPanel.setVisible(true);
		if (this.insertSupplierView != null) this.insertSupplierView.setVisible(false);
		//if (this.supplierView != null) this.supplierView.setVisible(false);
		
	}

	@Override
	public void quitModify() {
		//io arrivo qui da ModifySupplierView.close() ma prima da setModifySupplierController
		//(devo chiudere modifySupplierView)
		// e devo visualizzare supplierView
		this.supplierView.setVisible(true);
		if (this.modifySupplierView != null) this.modifySupplierView.setVisible(false);
		
	}
	@Override
	public void quitChoice() {
		//qui ci arrivo da ChoiceSupplier 
		//devo aggiornare la casella JText selezionata 
		//nascondere choiceSupplierView e visualizzare il JFrame precedente
		//che può essere InsertGameView o ModifyGame
		this.choiceFrame.setVisible(true);
		if (this.choiceSupplierView != null) this.choiceSupplierView.setVisible(false);
		
	}

	@Override
	public ArrayList<Supplier> getListSupplierView() {
		return modelCenter.getListSupplier();
	}

	@Override
	public void deleteSupplier(Supplier supplier) {
		modelCenter.getListSupplier().remove(supplier);
		supplierView.setPanel(getListSupplierView());
		
	}

	@Override
	public void setModifySupplierController(Supplier supplier, JLabel nameSupplier) {

		ModifySupplierViewImpl modifySupplierView = new ModifySupplierViewImpl();
		SupplierController supplierController = 
				new SupplierControllerImpl(this.modelCenter,this.supplierView,modifySupplierView );

		modifySupplierView.setData(supplier.getName(),
				supplier.getCodeSupplier(), nameSupplier);

		supplierView.setVisible(false);
		modifySupplierView.setVisible(true);

		modifySupplierView.addObserver(supplierController);

		
	}

}
