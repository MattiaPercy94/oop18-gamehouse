package model;

import java.util.ArrayList;

/**
 * @author Mattia Ferrari
 */

public interface VideogameCenter {

	/**
	 * this method add a new Section in the VideogameCenter
	 * 
	 * @param section
	 */
	public void addSection(Section section);

	/**
	 * this method return the number of Section in VideogameCenter
	 * 
	 * @return
	 */
	public int getNumberSection();

	/**
	 * this method return the list of Section in VideogameCenter
	 * 
	 * @return
	 */
	public ArrayList<Section> getListSection();

	/**
	 * this method delete a Section in VideogameCenter
	 * 
	 * @param section
	 */
	public void deleteSection(Section section);

	/**
	 * this method return the file with Section in VideogameCenter
	 * 
	 * @return
	 */
	ArrayList<Section> getListSectionFile();

	/**
	 * this method works with the file of Section
	 */
	public void insertSectionFile();
	
	//----------------------------------------------------------------------
	
	/**
	 * this method add a new Supplier in the VideogameCenter
	 * 
	 * @param Supplier
	 */
	public void addSupplier(Supplier supplier);

	/**
	 * this method return the number of Supplier in VideogameCenter
	 * 
	 * @return
	 */
	public int getNumberSupplier();

	/**
	 * this method return the list of Supplier in VideogameCenter
	 * 
	 * @return
	 */
	public ArrayList<Supplier> getListSupplier();

	/**
	 * this method delete a Supplier in VideogameCenter
	 * 
	 * @param supplier
	 */
	public void deleteSupplier(Supplier supplier);

	/**
	 * this method return the file with Supplier in VideogameCenter
	 * 
	 * @return
	 */
	ArrayList<Supplier> getListSupplierFile();

	/**
	 * this method works with the file of Supplier
	 */
	public void insertSupplierFile();
	

	/**
	 * this method works with the file of Log-In
	 * 
	 * @param username
	 * @param password
	 * 
	 * @return boolean(String username, String password)
	 */
	public boolean logIn(String username, String password);

}
