package model;

/**
 * @author Mattia Ferrari
 */

public interface Supplier {

	/**
	 * this method return the name of supplier
	 * 
	 * @return
	 */
	public String getName();

	/**
	 * this method return the code of supplier
	 * 
	 * @return
	 */
	public int getCodeSupplier();

	/**
	 * this method set a new name supplier
	 * 
	 * @param name
	 */
	public void setName(String name);

	/**
	 * this method set a new code supplier
	 * 
	 * @param code
	 */
	public void setCode(int code);

}
