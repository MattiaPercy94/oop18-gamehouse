package model;

import java.io.Serializable;

public class SupplierImpl implements Supplier, Serializable{

	private static final long serialVersionUID = 1L;
	private String name;
	private int codeSupplier;
	
	public SupplierImpl(String name, int code) {
		this.name=name;
		this.codeSupplier=code;
	}
	
	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public int getCodeSupplier() {
		return this.codeSupplier;
	}

	@Override
	public void setName(String name) {
		this.name=name;
		
	}

	@Override
	public void setCode(int code) {
		this.codeSupplier=code;
		
	}

}
