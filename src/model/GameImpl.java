package model;

import java.io.Serializable;

/**
 * @author Mattia Ferrari
 */

public class GameImpl implements Game, Serializable {

	private static final long serialVersionUID = 1L;
	private String name;
	private int codeGame;
	private int price;
	private int quantity;
	private String supplier;

	public GameImpl(String name, int code, int price, int quantity,String supplier ) {

		this.name = name;
		this.codeGame = code;
		this.price = price;
		this.quantity = quantity;
		this.supplier= supplier;
	}

	public String getName() {

		return this.name;
	}

	public int getCodeGame() {

		return this.codeGame;
	}

	public int getPrice() {

		return this.price;
	}

	public int getQuantity() {

		return this.quantity;
	}

	public void setName(String name) {

		this.name = name;
	}

	public void setPrice(int price) {

		this.price = price;
	}

	public void setQuantity(int quantity) {

		this.quantity = quantity;
	}

	public void setCode(int code) {

		this.codeGame = code;
	}

	@Override
	public String getSupplier() {
		return this.supplier;
	}

	@Override
	public void setSupplier(String supplier) {
		this.supplier=supplier;
		
	}

}
