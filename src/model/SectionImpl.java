
package model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author Mattia Ferrari
 */

public class SectionImpl implements Section, Serializable {

	private static final long serialVersionUID = 1L;
	private String name;
	private int codeSection;
	private int maxGameSection;
	private int sectionAmount;
	private ArrayList<Game> listGame;

	public SectionImpl(String name, int maxGameSection, int codeSection) {

		this.name = name;
		this.codeSection = codeSection;
		this.maxGameSection = maxGameSection;
		this.listGame = new ArrayList<Game>();

	}

	public void setName(String name) {

		this.name = name;

	}

	public void setMaxGameSection(int maxGameSection) {

		this.maxGameSection = maxGameSection;
	}

	public void setCode(int code) {

		this.codeSection = code;
	}

	public int getCodeSection() {

		return this.codeSection;

	}

	public int getMaxGameSection() {

		return this.maxGameSection;
	}

	public String getName() {

		return this.name;
	}

	public int getsectionAmount() {

		return this.sectionAmount;
	}

	public void insertGame(Game game) {

		listGame.add(game);

	}

	public int getListGameSize() {

		return listGame.size();
	}

	public ArrayList<Game> getListGame() {

		return listGame;

	}

	public void deleteGame(Game game) {

		listGame.remove(game);

	}
	
	public int quantityTotal(){
		
		int count = 0;
		
		
		for(int i=0; i<listGame.size(); i++){
				
				count = count + listGame.get(i).getQuantity();
			
		     }
		
		  System.out.println(count);
		  return count;
		}


}
