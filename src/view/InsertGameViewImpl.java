package view;

import javax.swing.*;

import Utility.Utility;
import model.Section;
import controller.GameController;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

/**
 * @author Mattia Ferrari
 *
 */
public class InsertGameViewImpl extends JFrame implements InsertGameView {

	private static final long serialVersionUID = 1L;
	GameController controller;
	//private JPanel mainPanel = new JPanel(new GridLayout(2, 6, 2, 2));
	private JPanel mainPanel = null ;
	
	private final JPanel internalMainPanel = new JPanel(new GridBagLayout());
	private final JButton backButton = new JButton("Indietro");
	private final JPanel bottomPanel = new JPanel(new GridLayout(0,3,2,2));
	private final JPanel topPanel = new JPanel(new GridBagLayout());
	
	private final JPanel insertPanel = new JPanel();
	private final JTextField nameGame = new JTextField(20);
	private final JLabel displayName = new JLabel("Inserire Nome:");
	private final JTextField codeGame = new JTextField(20);
	private final JLabel displaycode = new JLabel("Inserire Codice:");
	private final JTextField priceGame = new JTextField(20);
	private final JLabel displayprice = new JLabel("Inserire Prezzo:");
	private final JTextField quantity = new JTextField(20);
	private final JLabel displayquantity = new JLabel("Inserire Quantità:");
	private final JLabel checkString = new JLabel();
	private final JLabel titleLabel = new JLabel("Selezionare una sezione",SwingConstants.LEFT);
	private final JLabel title = new JLabel();
	private final JButton submitGame = new JButton("Inserisci");
	private final JButton cancel = new JButton("Indietro");
	
	private final JButton supplierChoice = new JButton("Scegli Fornitore");
	private final JTextField supplier = new JTextField(20);

	public InsertGameViewImpl(final MainPanel panel) {

		super("Inserisci Gioco");
		Utility.initFrameBegin(this);
		
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
//		this.setResizable(false);
//		this.setSize(700, 500);
//		this.setBounds(300, 175, this.getWidth(), this.getHeight());
//		this.setLocationRelativeTo(null);
//		Utility.centerFrame(this);
//		this.setVisible(true);
		
		
		
		this.setLayout(new BorderLayout());
		this.mainPanel=(JPanel) this.getContentPane();
		
		
		topPanel.setBackground(new Color(192, 192, 192));
		GridBagConstraints c = new GridBagConstraints();	
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1;
		c.gridx = 0;
		c.gridy = 0;
		topPanel.add(titleLabel,c);
		
		
		
		bottomPanel.setBackground(new Color(192, 192, 192));
		bottomPanel.add(new JLabel());
		bottomPanel.add(new JLabel());
		bottomPanel.add(backButton);

		// this.getContentPane().setLayout(new BorderLayout());

		//mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
		internalMainPanel.setBackground(new Color(192, 192, 192));
		JScrollPane scroll = new JScrollPane(internalMainPanel,	JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);


//		this.add(scroll, BorderLayout.CENTER);
//		this.getContentPane().add(mainPanel);
//		mainPanel.add(titlePanel);
		
		this.add(topPanel,BorderLayout.NORTH);
		this.add(scroll,BorderLayout.CENTER);
		this.add(bottomPanel,BorderLayout.SOUTH);
		
		
		
		
		

		//titlePanel.setFont(Utility.fontTitle);
	

		SpringLayout spring = new SpringLayout();
		insertPanel.setLayout(spring);
		
		insertPanel.setBackground(new Color(192, 192, 192));

		insertPanel.add(nameGame);
		spring.putConstraint(SpringLayout.NORTH, nameGame, 80,
				SpringLayout.NORTH, insertPanel);
		spring.putConstraint(SpringLayout.WEST, nameGame, 250,
				SpringLayout.WEST, insertPanel);
		insertPanel.add(codeGame);
		spring.putConstraint(SpringLayout.NORTH, codeGame, 120,
				SpringLayout.NORTH, insertPanel);
		spring.putConstraint(SpringLayout.WEST, codeGame, 250,
				SpringLayout.WEST, insertPanel);
		insertPanel.add(priceGame);
		spring.putConstraint(SpringLayout.NORTH, priceGame, 160,
				SpringLayout.NORTH, insertPanel);
		spring.putConstraint(SpringLayout.WEST, priceGame, 250,
				SpringLayout.WEST, insertPanel);
		insertPanel.add(quantity);
		spring.putConstraint(SpringLayout.NORTH, quantity, 200,
				SpringLayout.NORTH, insertPanel);
		spring.putConstraint(SpringLayout.WEST, quantity, 250,
				SpringLayout.WEST, insertPanel);
		
		insertPanel.add(supplier);
		supplier.setEditable(false);
		spring.putConstraint(SpringLayout.NORTH, supplier, 240,
				SpringLayout.NORTH, insertPanel);
		spring.putConstraint(SpringLayout.WEST, supplier, 250,
				SpringLayout.WEST, insertPanel);
		
		//pulsanti finali
		insertPanel.add(submitGame);
		spring.putConstraint(SpringLayout.NORTH, submitGame, 280,
				SpringLayout.NORTH, insertPanel);
		spring.putConstraint(SpringLayout.WEST, submitGame, 250,
				SpringLayout.WEST, insertPanel);
		insertPanel.add(checkString);
		spring.putConstraint(SpringLayout.NORTH, checkString, 370,
				SpringLayout.NORTH, insertPanel);
		spring.putConstraint(SpringLayout.WEST, checkString, 250,
				SpringLayout.WEST, insertPanel);
		insertPanel.add(cancel);
		spring.putConstraint(SpringLayout.NORTH, cancel, 280,
				SpringLayout.NORTH, insertPanel);
		spring.putConstraint(SpringLayout.WEST, cancel, 330, SpringLayout.WEST,
				insertPanel);

		
		insertPanel.add(title);
		spring.putConstraint(SpringLayout.NORTH, title, 40, SpringLayout.NORTH,
				insertPanel);
		spring.putConstraint(SpringLayout.WEST, title, 200, SpringLayout.WEST,
				insertPanel);

		insertPanel.add(displayName);
		spring.putConstraint(SpringLayout.NORTH, displayName, 80,
				SpringLayout.NORTH, insertPanel);
		spring.putConstraint(SpringLayout.WEST, displayName, 100,
				SpringLayout.WEST, insertPanel);
		insertPanel.add(displaycode);
		spring.putConstraint(SpringLayout.NORTH, displaycode, 120,
				SpringLayout.NORTH, insertPanel);
		spring.putConstraint(SpringLayout.WEST, displaycode, 100,
				SpringLayout.WEST, insertPanel);
		insertPanel.add(displayprice);
		spring.putConstraint(SpringLayout.NORTH, displayprice, 160,
				SpringLayout.NORTH, insertPanel);
		spring.putConstraint(SpringLayout.WEST, displayprice, 100,
				SpringLayout.WEST, insertPanel);
		insertPanel.add(displayquantity);
		spring.putConstraint(SpringLayout.NORTH, displayquantity, 200,
				SpringLayout.NORTH, insertPanel);
		spring.putConstraint(SpringLayout.WEST, displayquantity, 100,
				SpringLayout.WEST, insertPanel);
		
		insertPanel.add(supplierChoice);
		spring.putConstraint(SpringLayout.NORTH, supplierChoice, 240,
				SpringLayout.NORTH, insertPanel);
		spring.putConstraint(SpringLayout.WEST, supplierChoice, 100,
				SpringLayout.WEST, insertPanel);

		this.addWindowListener(new WindowAdapter() {

			public void windowClosing(final WindowEvent e) {

				controller.quit();

			}

		});
		
		backButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				controller.quit();

			}

		});
		
		this.actionEventMainPanel();
		
		Utility.initFrameEnd(this);
		

	}

	public void addObserver(GameController controller) {
		
		this.controller = controller;

		listSection(controller.getCenter().getListSection());

	}

	public void listSection(ArrayList<Section> section) {
		GridBagConstraints c = new GridBagConstraints();
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 0;
		internalMainPanel.add(new JLabel("Nome Sezione",SwingConstants.LEFT),c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 1;
		c.gridy = 0;
		internalMainPanel.add(new JLabel("Seleziona",SwingConstants.LEFT),c);
		
		for (int i = 0; i < section.size(); i++) {
			
			JLabel newLabel = new JLabel(section.get(i).getName());
			newLabel.setHorizontalAlignment(SwingConstants.LEFT);

			JButton selectButton = new JButton("Seleziona Sezione");
			selectButton.setHorizontalAlignment(SwingConstants.LEFT);
			
			c.ipady=0;
			c.weightx = 0.5;
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = i+1;
			internalMainPanel.add(newLabel,c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 1;
			c.gridy = i+1;
			internalMainPanel.add(selectButton,c);

			

			//JButton newButton = new JButton(section.get(i).getName());
//			mainPanel.add(newButton);
//			newButton.setMaximumSize(new Dimension(100, 100));
//			newButton.setFont(Utility.fontDisplay);
			actionEventChoiceSection(i, selectButton, section);

		}
		c.fill = GridBagConstraints.HORIZONTAL;
		//c.ipady=40;
		c.weightx=0;
		c.weighty=100;
		c.gridwidth = 3;
		//c.gridheight = 20;
		c.gridx = 0;
		c.gridy = section.size()+1;
		internalMainPanel.add(new JLabel(),c);
		validate();

	}

	private void actionEventMainPanel() {
		
		supplierChoice.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
					//JOptionPane.showMessageDialog(InsertGameViewImpl.this, "Implement Choice");
				controller.ChoiceSupplierView(supplier, InsertGameViewImpl.this);
			}
			
		});
		
		cancel.addActionListener(new ActionListener() {
		
			@Override
			public void actionPerformed(ActionEvent e) {
		
				//insertPanel.setVisible(false);
				////setPanel(mainPanel);
				//mainPanel.setVisible(true);	
					setPanel(mainPanel);
			
					uneableJtextField();
			
				}
		
		});

		
		
		
	}
	
	private void actionEventChoiceSection(int index, JButton newButton,ArrayList<Section> section) {

		newButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

//				mainPanel.setVisible(false);
//				insertPanel.setVisible(true);
				setPanel(insertPanel);
				title.setText("Inserisci elemento nella Sezione : " + section.get(index).getName());
				uneableJtextField();
				validate();
				
				//questo pulsante deve agire con i parametri dell'azione voluta
				//però ogli volta che seleziono una sezione aggiunge un listener
				//quindi devo rimuovere il listener precedente prima di visualizzare
				//la relativa submit, gli altri pulsanti cancel e choiceSupplier 
				//non dipendono dalla sezione selezionata e quindi vengono gestiti 
				//con eventi unici impostati nel costruttore
				if (submitGame.getActionListeners().length>0) {
//					for (ActionListener a :submitGame.getActionListeners() ) {
//						submitGame.removeActionListener(a);
//					}
					for(;submitGame.getActionListeners().length>0;) {
						submitGame.removeActionListener(submitGame.getActionListeners()[0]);
					}
				}
				
				
				submitGame.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
				
						if (codeGame.getText().isEmpty()
								|| nameGame.getText().isEmpty()
								|| priceGame.getText().isEmpty()
								|| quantity.getText().isEmpty()) {
				
							//checkString.setText(Utility.ERRORDATA);
							JOptionPane.showMessageDialog(InsertGameViewImpl.this,Utility.ERRORDATA);
				
						} else if (!codeGame.getText().matches("[+]?\\d*\\.?\\d+")
								|| !quantity.getText().matches("[+]?\\d*\\.?\\d+")
								|| !priceGame.getText().matches("[+]?\\d*\\.?\\d+")) {
				
							//checkString.setText(Utility.ERRORCODEQUANTIY);
							JOptionPane.showMessageDialog(InsertGameViewImpl.this,Utility.ERRORCODEQUANTIY);
				
						} else if (controller.checkCode(Integer.parseInt(codeGame.getText()))) {
				
							//checkString.setText(Utility.ERRORCODE);
							JOptionPane.showMessageDialog(InsertGameViewImpl.this,Utility.ERRORCODE);
				
						} else if (controller.checkName(nameGame.getText())) {
				
							//checkString.setText(Utility.ERRORNAME);
							JOptionPane.showMessageDialog(InsertGameViewImpl.this,Utility.ERRORNAME);
				
						} else {
							/*
							checkString.setText(
							
									controller.insertGame(
										section.get(index),
										nameGame.getText(),
										Integer.parseInt(codeGame.getText()),
										index,
										Integer.parseInt(priceGame.getText()),
										Integer.parseInt(quantity.getText()),
										supplier.getText()
										)
									);
							*/
							
							
							JOptionPane.showMessageDialog(InsertGameViewImpl.this,
										controller.insertGame(
												section.get(index),
												nameGame.getText(),
												Integer.parseInt(codeGame.getText()),
												index,
												Integer.parseInt(priceGame.getText()),
												Integer.parseInt(quantity.getText()),
												supplier.getText()
												)
									
									);
							
							uneableJtextField();
				
						}
				
					}
				
				});

			}

		});
		
		

	}

	private void uneableJtextField() {

		nameGame.setText("");
		codeGame.setText("");
		priceGame.setText("");
		quantity.setText("");
		supplier.setText("");

	}

	private void setPanel(JPanel panel) {
		
		this.setContentPane(panel);
		//this.getContentPane().add(panel);
	}
}
