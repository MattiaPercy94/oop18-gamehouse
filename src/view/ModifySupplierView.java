package view;

import javax.swing.JLabel;

import controller.SupplierController;

/**
 * @author Mattia Ferrari
 */

public interface ModifySupplierView {

	/**
	 * this method use the pattern Observer
	 * 
	 * @param controller
	 */
	public void addObserver(SupplierController controller);

	/**
	 * this method use set the data of the GUI
	 * 
	 * @param name
	 * @param code
	 * @param nameSupplier
	 */
	public void setData(String name, int code, JLabel nameSupplier);

}
