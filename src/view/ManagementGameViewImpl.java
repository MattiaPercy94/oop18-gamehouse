package view;

import model.Game;
import controller.GameController;

import javax.swing.*;

import Utility.Utility;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

/**
 * @author Mattia Ferrari
 */

public class ManagementGameViewImpl extends JFrame implements ManagementGameView {

	private static final long serialVersionUID = 1L;
	//private final JPanel mainPanel = new JPanel(new GridLayout(0, 7, 2, 2));
	private final JPanel mainPanel = new JPanel(new GridBagLayout());
	private final JButton backButton = new JButton("Indietro");
	private final JPanel bottomPanel = new JPanel(new GridLayout(0,3,2,2));
	private final JPanel topPanel = new JPanel(new GridBagLayout());
	
	
	//private final JPanel componentPanel = new JPanel(new GridLayout(0, 7, 2, 2));
	private final JLabel printGame = new JLabel("Giochi",SwingConstants.LEFT);
	private final JLabel displayModify = new JLabel("Modifica",SwingConstants.LEFT);
	private final JLabel displayDelete = new JLabel("Elimina",SwingConstants.LEFT);
	private final JLabel displayPrice = new JLabel("Prezzo",SwingConstants.LEFT);
	private final JLabel displayTotalPrice = new JLabel("Prezzo totale",SwingConstants.LEFT);
	private final JLabel displayQuantity = new JLabel("Quantità",SwingConstants.LEFT);
	private final JLabel displaySupplier = new JLabel("Fornitore",SwingConstants.LEFT);
	GameController controller;

	public ManagementGameViewImpl() {
		super("Gestione Giochi");
		Utility.initFrameBegin(this);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

//		this.setResizable(false);
//		this.setSize(700, 500);
//		this.setBounds(300, 175, this.getWidth(), this.getHeight());
//		this.setLocationRelativeTo(null);
//		Utility.centerFrame(this);
//		this.setVisible(true);
		
		this.setLayout(new BorderLayout());

		
		topPanel.setBackground(new Color(192, 192, 192));

		
		bottomPanel.setBackground(new Color(192, 192, 192));
		bottomPanel.add(new JLabel());
		bottomPanel.add(new JLabel());
		bottomPanel.add(backButton);
		
		mainPanel.setBackground(new Color(192, 192, 192));

		JScrollPane scroll = new JScrollPane(mainPanel,	JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	
		
		this.add(topPanel,BorderLayout.NORTH);
		this.add(scroll,BorderLayout.CENTER);
		this.add(bottomPanel,BorderLayout.SOUTH);
		
		
		
		
		
//		this.add(mainPanel, BorderLayout.NORTH);
//		mainPanel.setBackground(new Color(192, 192, 192));
//		componentPanel.setBackground(new Color(192, 192, 192));
//
//		mainPanel.add(componentPanel);
//
//		JScrollPane scroll = new JScrollPane(componentPanel,
//				JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
//				JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
//
//		this.add(scroll);
//
//		mainPanel.add(printGame);
//
//		mainPanel.add(displayPrice);
//		displayPrice.setFont(Utility.Utility.fontDisplay);
//		mainPanel.add(displayQuantity);
//		displayQuantity.setFont(Utility.Utility.fontDisplay);
//		mainPanel.add(displayTotalPrice);
//		displayTotalPrice.setFont(Utility.Utility.fontDisplay);
//		mainPanel.add(displaySupplier);
//		displaySupplier.setFont(Utility.Utility.fontDisplay);
//		mainPanel.add(displayModify);
//		displayModify.setFont(Utility.Utility.fontDisplay);
//		mainPanel.add(displayDelete);
//		displayDelete.setFont(Utility.Utility.fontDisplay);
//
//		printGame.setVisible(false);

		this.addWindowListener(new WindowAdapter() {

			public void windowClosing(final WindowEvent e) {

				controller.quitModify();

			}

		});
		backButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				controller.quitModify();

			}

		});

		Utility.initFrameEnd(this);
		
	}

	

	public void listGame(ArrayList<Game> game) {
		

		
		GridBagConstraints c = new GridBagConstraints();
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 0;
		mainPanel.add(printGame,c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 1;
		c.gridy = 0;
		mainPanel.add(displayPrice,c);
		

		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 2;
		c.gridy = 0;
		mainPanel.add(displayQuantity,c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 3;
		c.gridy = 0;
		mainPanel.add(displayTotalPrice,c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 4;
		c.gridy = 0;
		mainPanel.add(displaySupplier,c);
		

		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 5;
		c.gridy = 0;
		mainPanel.add(displayModify,c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 6;
		c.gridy = 0;
		mainPanel.add(displayDelete,c);
		
		

		for (int i = 0; i < game.size(); i++) {

			JLabel nameGame = new JLabel(game.get(i).getName());
			JLabel priceGame = new JLabel("€" + game.get(i).getPrice());
			JLabel quantityGame = new JLabel(""
					+ game.get(i).getQuantity());
			JLabel totalPriceGame = new JLabel("€"
					+ game.get(i).getQuantity() * game.get(i).getPrice());
			JLabel  supplier = new JLabel(game.get(i).getSupplier());
			JButton deleteGame = new JButton("Elimina");
			JButton modifyGame = new JButton("Modifica");

			c.ipady=0;
			c.weightx = 0.5;
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = i+1;
			mainPanel.add(nameGame,c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 1;
			c.gridy = i+1;
			mainPanel.add(priceGame,c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 2;
			c.gridy = i+1;
			mainPanel.add(quantityGame,c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 3;
			c.gridy = i+1;
			mainPanel.add(totalPriceGame,c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 4;
			c.gridy = i+1;
			mainPanel.add(supplier,c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 5;
			c.gridy = i+1;
			mainPanel.add(modifyGame,c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 6;
			c.gridy = i+1;
			mainPanel.add(deleteGame,c);
			
			
			
//			componentPanel.add(nameGame);
//			componentPanel.add(priceGame);
//			componentPanel.add(quantityGame);
//			componentPanel.add(totalPriceGame);
//			componentPanel.add(supplier);
//			componentPanel.add(modifyGame);
//			componentPanel.add(deleteGame);

			/*
			 * Qui per il metodo quit ho utilzzato il this nel metodo action
			 * listener
			 */
			actionEvent(deleteGame, game.get(i), modifyGame, this);
			validate();

		}
		c.fill = GridBagConstraints.HORIZONTAL;
		//c.ipady=40;
		c.weightx=0;
		c.weighty=100;
		c.gridwidth = 3;
		//c.gridheight = 20;
		c.gridx = 0;
		c.gridy = game.size()+1;
		mainPanel.add(new JLabel(),c);
		validate();

	}

	public void actionEvent(JButton deleteGame, Game game,JButton modifySection, ManagementGameViewImpl view) {

		modifySection.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				controller.modifyController(game, view);

			}

		});

		deleteGame.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				controller.deleteGame(game);
				if ( controller.getSection().getListGameSize() <=0 ) {
					controller.quitModify();
				}

			}

		});

	}

	public void setPanel(ArrayList<Game> game) {
		mainPanel.removeAll();
		mainPanel.setSize(10, 10);
		//componentPanel.removeAll();
		listGame(game);
		
		validate();

	}
	
	
	public void addObserver(GameController controller) {

		this.controller = controller;
		
		GridBagConstraints c = new GridBagConstraints();
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1;
		c.gridx = 0;
		c.gridy = 0;
		this.topPanel.add(new JLabel("Giochi della sezione: "+this.controller.getSection().getName(),SwingConstants.LEFT),c);
	}

}
