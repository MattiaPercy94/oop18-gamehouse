package view;

import javax.swing.*;

import Utility.Utility;
import controller.SectionController;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * @author Mattia Ferrari
 * 
 */
public class InsertSectionImpl extends JFrame implements InsertSection {

	private static final long serialVersionUID = 1L;
	private final JPanel mainPanel = new JPanel();
	private final JButton insertSection = new JButton("Inserisci");
	private final JButton close = new JButton("Indietro");
	private final JTextField sectioName = new JTextField(20);
	private final JTextField sectioMaxGame = new JTextField(20);
	private final JTextField sectioCode = new JTextField(20);
	private final JLabel titlePanel = new JLabel("Inserimento Nuova Sezione");
	private final JLabel displayName = new JLabel("Inserisci Nome :");
	private final JLabel displayMaxGame = new JLabel(
			"Inserisci max quantità giochi :");
	private final JLabel displayCode = new JLabel("Inserisci codice sezione :");
	private SectionController sectionController;
	private final JLabel checkString = new JLabel();

	public InsertSectionImpl() {

		super("Inserisci sezione");
		Utility.initFrameBegin(this);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

//		this.setResizable(false);
//		this.setSize(700, 500);
//		this.setBounds(300, 175, this.getWidth(), this.getHeight());
//		this.setLocationRelativeTo(null);
//		Utility.centerFrame(this);
//		this.setVisible(true);
		
		
		SpringLayout spring = new SpringLayout();
		mainPanel.setLayout(spring);

		this.add(mainPanel);

		

		mainPanel.add(titlePanel);
		titlePanel.setFont(Utility.fontTitle);
		mainPanel.setBackground(new Color(192, 192, 192));
		spring.putConstraint(SpringLayout.NORTH, titlePanel, 10,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, titlePanel, 40,
				SpringLayout.WEST, this.getContentPane());

		mainPanel.add(displayName);
		displayName.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, displayName, 100,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, displayName, 40,
				SpringLayout.WEST, this.getContentPane());
		mainPanel.add(displayMaxGame);
		displayMaxGame.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, displayMaxGame, 180,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, displayMaxGame, 40,
				SpringLayout.WEST, this.getContentPane());
		mainPanel.add(displayCode);
		displayCode.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, displayCode, 260,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, displayCode, 40,
				SpringLayout.WEST, this.getContentPane());

		mainPanel.add(sectioName);
		spring.putConstraint(SpringLayout.NORTH, sectioName, 100,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, sectioName, 280,
				SpringLayout.WEST, this.getContentPane());
		mainPanel.add(sectioCode);
		spring.putConstraint(SpringLayout.NORTH, sectioCode, 260,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, sectioCode, 280,
				SpringLayout.WEST, this.getContentPane());
		mainPanel.add(sectioMaxGame);
		spring.putConstraint(SpringLayout.NORTH, sectioMaxGame, 180,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, sectioMaxGame, 280,
				SpringLayout.WEST, this.getContentPane());

		mainPanel.add(checkString);
		spring.putConstraint(SpringLayout.NORTH, checkString, 330,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, checkString, 280,
				SpringLayout.WEST, this.getContentPane());
		
		mainPanel.add(insertSection);
		insertSection.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, insertSection, 300,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, insertSection, 280,
				SpringLayout.WEST, this.getContentPane());
		mainPanel.add(close);
		close.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, close, 300,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, close, 400, SpringLayout.WEST,
				this.getContentPane());

		close.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				sectionController.quit();

			}

		});

		insertSection.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (sectioCode.getText().isEmpty()
						|| sectioMaxGame.getText().isEmpty()
						|| sectioName.getText().isEmpty()) {

					//checkString.setText(Utility.ERRORDATA);
					JOptionPane.showMessageDialog(InsertSectionImpl.this,Utility.ERRORDATA);

				} else if (!sectioCode.getText().matches("[+]?\\d*\\.?\\d+")
						|| !sectioMaxGame.getText().matches(
								"[+]?\\d*\\.?\\d+")) {

					//checkString.setText(Utility.ERRORCODEQUANTIY);
					JOptionPane.showMessageDialog(InsertSectionImpl.this,Utility.ERRORCODEQUANTIY);

				} else if (sectionController.checkCode(Integer
						.parseInt(sectioCode.getText())) == true) {

					//checkString.setText(Utility.ERRORCODE);
					JOptionPane.showMessageDialog(InsertSectionImpl.this,Utility.ERRORCODE);
					
				} else if (sectionController.checkName(sectioName.getText()) == true) {

					//checkString.setText(Utility.ERRORNAME);
					JOptionPane.showMessageDialog(InsertSectionImpl.this,Utility.ERRORNAME);

				} else {
						
					
					sectionController.insertSection(
							sectioName.getText(),
							Integer.parseInt(sectioMaxGame.getText()),
							Integer.parseInt(sectioCode.getText()));

					//checkString.setText(Utility.SUCCESSINSERT);
					JOptionPane.showMessageDialog(InsertSectionImpl.this,Utility.SUCCESSINSERT);
					
					
					sectioName.setText("");
					sectioMaxGame.setText("");
					sectioCode.setText("");

				}

			}

		});

		this.addWindowListener(new WindowAdapter() {

			public void windowClosing(final WindowEvent e) {

				sectionController.quit();

			}

		});
		
		Utility.initFrameEnd(this);

	}

	public void addObserver(SectionController controller) {

		this.sectionController = controller;
	}

}
