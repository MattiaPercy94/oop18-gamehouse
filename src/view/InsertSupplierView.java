package view;

import controller.SupplierController;

/**
 * @author Mattia Ferrari
 */

public interface InsertSupplierView {

	/**
	 * this method use the pattern Observer
	 * 
	 * @param controller
	 */
	public void addObserver(SupplierController controller);

}
