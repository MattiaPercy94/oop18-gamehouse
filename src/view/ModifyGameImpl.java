package view;

import javax.swing.*;

//import Utility.Utility;
import controller.GameController;
import Utility.Utility;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * @author Mattia Ferrari
 */

public class ModifyGameImpl extends JFrame implements ModifyGame {

	private static final long serialVersionUID = 1L;
	private final JPanel mainPanel = new JPanel();
	private final JLabel displayName = new JLabel("Nome :");
	private final JLabel displayPrice = new JLabel("Prezzo :");
	private final JLabel displayQuantity = new JLabel("Quantità :");
	private final JLabel title = new JLabel("Modifica Gioco");
	private final JTextField gameName = new JTextField(20);
	private final JTextField priceGame = new JTextField(20);
	private final JTextField quantityGame = new JTextField(20);
	private int codeGame;
	
	private final JButton supplierChoice = new JButton("Scegli Fornitore");
	private final JTextField supplier = new JTextField(20);
	
	
	private JButton save = new JButton("Salva");
	private JButton back = new JButton("Indietro");
	private JLabel labelCheck = new JLabel();
	GameController controller;

	public ModifyGameImpl(ManagementGameViewImpl view) {
		super("Modifica gioco");
		Utility.initFrameBegin(this);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

//		this.setResizable(false);
//		this.setSize(700, 500);
//		this.setBounds(300, 175, this.getWidth(), this.getHeight());
//		this.setLocationRelativeTo(null);
//		Utility.centerFrame(this);
//		this.setVisible(true);

		SpringLayout spring = new SpringLayout();

		mainPanel.setBackground(new Color(192, 192, 192));

		mainPanel.setLayout(spring);

		this.add(mainPanel);

		mainPanel.add(title);
		title.setFont(Utility.fontTitle);
		spring.putConstraint(SpringLayout.NORTH, title, 10, SpringLayout.NORTH,
				this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, title, 40, SpringLayout.WEST,
				this.getContentPane());
		mainPanel.add(displayName);
		displayName.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, displayName, 100,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, displayName, 40,
				SpringLayout.WEST, this.getContentPane());
		mainPanel.add(gameName);
		gameName.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, gameName, 100,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, gameName, 280,
				SpringLayout.WEST, this.getContentPane());
		mainPanel.add(displayPrice);
		displayPrice.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, displayPrice, 140,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, displayPrice, 40,
				SpringLayout.WEST, this.getContentPane());
		mainPanel.add(priceGame);
		priceGame.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, priceGame, 140,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, priceGame, 280,
				SpringLayout.WEST, this.getContentPane());
		mainPanel.add(displayQuantity);
		displayQuantity.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, displayQuantity, 180,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, displayQuantity, 40,
				SpringLayout.WEST, this.getContentPane());
		mainPanel.add(quantityGame);
		quantityGame.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, quantityGame, 180,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, quantityGame, 280,
				SpringLayout.WEST, this.getContentPane());
		
		mainPanel.add(supplierChoice);
		displayQuantity.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, supplierChoice, 220,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, supplierChoice, 40,
				SpringLayout.WEST, this.getContentPane());
		mainPanel.add(supplier);
		supplier.setEditable(false);
		supplier.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, supplier, 220,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, supplier, 280,
				SpringLayout.WEST, this.getContentPane());
		
		
		
		
		mainPanel.add(save);
		save.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, save, 260, SpringLayout.NORTH,	this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, save, 240, SpringLayout.WEST,this.getContentPane());
		
		mainPanel.add(back);
		back.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, back, 260, SpringLayout.NORTH,	this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, back, 320, SpringLayout.WEST,this.getContentPane());
		

		mainPanel.add(labelCheck);
		labelCheck.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, labelCheck, 300,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, labelCheck, 280,
				SpringLayout.WEST, this.getContentPane());
		
		
		supplierChoice.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				//JOptionPane.showMessageDialog(ModifyGameImpl.this, "Implement Choice");
				controller.ChoiceSupplierView(supplier, ModifyGameImpl.this);
			}

		});
		
		save.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				String check;

				try {
					check = controller.changeGame(codeGame,
							gameName.getText(),
							Integer.parseInt(priceGame.getText()),
							Integer.parseInt(quantityGame.getText()),supplier.getText());

					//labelCheck.setText(check);
					JOptionPane.showMessageDialog(ModifyGameImpl.this,check);
					view.setVisible(false);

				} catch (NumberFormatException e) {

					check = Utility.ERRORFORMAT;
					//labelCheck.setText(check);
					JOptionPane.showMessageDialog(ModifyGameImpl.this,check);
					
				}

			}

		});
		
		ModifyGameImpl viewGame = this;
		
		back.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				controller.quitModifyGame(viewGame);

			}

		});

		

		this.addWindowListener(new WindowAdapter() {

			public void windowClosing(final WindowEvent e) {

				controller.quitModifyGame(viewGame);

			}

		});
		Utility.initFrameEnd(this);

	}

	public void addObserver(GameController controller) {

		this.controller = controller;
	}

	public void setData(int code, String name, int price, int quantity,String supplier) {

		gameName.setText(name);
		priceGame.setText(String.valueOf(price));
		quantityGame.setText(String.valueOf(quantity));
		this.codeGame = code;
		this.supplier.setText(supplier);

	}

}
