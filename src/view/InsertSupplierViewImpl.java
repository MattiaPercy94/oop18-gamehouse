package view;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SpringLayout;

import Utility.Utility;
import controller.SupplierController;

public class InsertSupplierViewImpl extends JFrame implements InsertSupplierView {

	private static final long serialVersionUID = 1L;
	private final JPanel mainPanel = new JPanel();
	private final JButton insertSupplier = new JButton("Inserisci");
	private final JButton close = new JButton("Indietro");
	private final JTextField supplierName = new JTextField(20);
	private final JTextField supplierCode = new JTextField(20);
	private final JLabel titlePanel = new JLabel("Inserimento Nuovo Fornitore");
	private final JLabel displayName = new JLabel("Inserisci Nome :");
	private final JLabel displayCode = new JLabel("Inserisci codice fornitore :");
	private SupplierController supplierController;
	private final JLabel checkString = new JLabel();
	

	public InsertSupplierViewImpl() {

		super("Inserisci fornitore");
		Utility.initFrameBegin(this);

		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

//		this.setResizable(false);
//		this.setSize(700, 500);
//		this.setBounds(300, 175, this.getWidth(), this.getHeight());
//		this.setLocationRelativeTo(null);
//		Utility.centerFrame(this);
//		this.setVisible(true);

		SpringLayout spring = new SpringLayout();
		mainPanel.setLayout(spring);

		this.add(mainPanel);

		mainPanel.add(titlePanel);
		titlePanel.setFont(Utility.fontTitle);
		mainPanel.setBackground(new Color(192, 192, 192));
		spring.putConstraint(SpringLayout.NORTH, titlePanel, 10,SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, titlePanel, 40,	SpringLayout.WEST, this.getContentPane());

		mainPanel.add(displayName);
		displayName.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, displayName, 100,SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, displayName, 40,SpringLayout.WEST, this.getContentPane());
		
		mainPanel.add(supplierName);
		spring.putConstraint(SpringLayout.NORTH, supplierName, 100,	SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, supplierName, 280, SpringLayout.WEST, this.getContentPane());
		
		mainPanel.add(displayCode);
		displayCode.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, displayCode, 200,SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, displayCode, 40,SpringLayout.WEST, this.getContentPane());
		
		mainPanel.add(supplierCode);
		spring.putConstraint(SpringLayout.NORTH, supplierCode, 200,	SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, supplierCode, 280,	SpringLayout.WEST, this.getContentPane());

		
		
		

		mainPanel.add(checkString);
		spring.putConstraint(SpringLayout.NORTH, checkString, 330,	SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, checkString, 280,SpringLayout.WEST, this.getContentPane());

		mainPanel.add(insertSupplier);
		insertSupplier.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, insertSupplier, 300,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, insertSupplier, 280,
				SpringLayout.WEST, this.getContentPane());
		mainPanel.add(close);
		close.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, close, 300,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, close, 400, SpringLayout.WEST,
				this.getContentPane());

		close.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				supplierController.quitInsert();

			}

		});

		insertSupplier.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if (supplierCode.getText().isEmpty()
						|| supplierName.getText().isEmpty()) {

					//checkString.setText(Utility.ERRORDATA);
					JOptionPane.showMessageDialog(InsertSupplierViewImpl.this,Utility.ERRORDATA);

				} else if (!supplierCode.getText().matches("[+]?\\d*\\.?\\d+")) {

					//checkString.setText(Utility.ERRORCODE);
					JOptionPane.showMessageDialog(InsertSupplierViewImpl.this,Utility.ERRORCODE2);
				}
					else if (supplierController.checkCode(Integer.parseInt(supplierCode.getText()))							 ) {
						//checkString.setText(Utility.ERRORCODE);
						JOptionPane.showMessageDialog(InsertSupplierViewImpl.this,Utility.ERRORCODE);
						

				} else if (supplierController.checkName(supplierName.getText()) == true) {

					//checkString.setText(Utility.ERRORNAME);
					JOptionPane.showMessageDialog(InsertSupplierViewImpl.this,Utility.ERRORNAME);

				} else {

					supplierController.insertSupplier(
							supplierName.getText(),
							Integer.parseInt(supplierCode.getText()));

					//checkString.setText(Utility.SUCCESSINSERT);
					JOptionPane.showMessageDialog(InsertSupplierViewImpl.this,Utility.SUCCESSINSERT);

					supplierName.setText("");
					supplierCode.setText("");

				}

			}

		});

		this.addWindowListener(new WindowAdapter() {

			public void windowClosing(final WindowEvent e) {

				supplierController.quitInsert();

			}

		});

		Utility.initFrameEnd(this);
	}


	
	
	@Override
	public void addObserver(SupplierController controller) {
		
		this.supplierController = controller;
	}

}
