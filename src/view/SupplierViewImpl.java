package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import Utility.Utility;
import controller.SupplierController;
import model.Supplier;

public class SupplierViewImpl  extends JFrame  implements SupplierView {

	private static final long serialVersionUID = 1L;
	SupplierController controller;
	
	private final JPanel supplierPanel = new JPanel(new GridBagLayout());
	private final JButton backButton = new JButton("Indietro");
	private final JPanel bottomPanel = new JPanel(new GridLayout(0,3,2,2));
	private final JPanel topPanel = new JPanel(new GridBagLayout());
	
	private final JLabel displayDelete = new JLabel("Elimina",SwingConstants.LEFT);
	private final JLabel displayModify = new JLabel("Modifica",SwingConstants.LEFT);
	private final JLabel displayName = new JLabel("Nome Fornitore",SwingConstants.LEFT);
	
	
	
	public SupplierViewImpl() {
		super("Fornitori");
		Utility.initFrameBegin(this);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

//		this.setResizable(false);
//		this.setSize(700, 500);
//		this.setBounds(300, 175, this.getWidth(), this.getHeight());
//		this.setLocationRelativeTo(null);
//		Utility.centerFrame(this);
//		this.setVisible(true);
		
		
		
		this.setLayout(new BorderLayout());
		
		
		topPanel.setBackground(new Color(192, 192, 192));

		
		bottomPanel.setBackground(new Color(192, 192, 192));
		bottomPanel.add(new JLabel());
		bottomPanel.add(new JLabel());
		bottomPanel.add(backButton);
		
		supplierPanel.setBackground(new Color(192, 192, 192));

		JScrollPane scroll = new JScrollPane(supplierPanel,	JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
	
		
		this.add(topPanel,BorderLayout.NORTH);
		this.add(scroll,BorderLayout.CENTER);
		this.add(bottomPanel,BorderLayout.SOUTH);
		
		

		this.addWindowListener(new WindowAdapter() {

			public void windowClosing(final WindowEvent e) {

				controller.quit();

			}

		});
		
		backButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				controller.quit();

			}

		});
		Utility.initFrameEnd(this);
	}

	@Override
	public void actionEvent(JButton deleteSupplier, Supplier supplier, JButton modifySupplier, JLabel nameSupplier,
			SupplierViewImpl view) {
		deleteSupplier.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				final int exit = JOptionPane.showConfirmDialog(view,
						Utility.QUESTIONDELETE2, "Cancellazione",
						JOptionPane.YES_NO_OPTION);
				if (exit == JOptionPane.YES_OPTION) {

					controller.deleteSupplier(supplier);
					if (controller.getListSupplierView().size()==0) controller.quit();

				}

			}

		});

		modifySupplier.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				controller.setModifySupplierController(supplier, nameSupplier);

			}

		});
		
	}

	@Override
	public void listSupplier(ArrayList<Supplier> supplier) {
		GridBagConstraints c = new GridBagConstraints();
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 0;
		supplierPanel.add(displayName,c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 1;
		c.gridy = 0;
		supplierPanel.add(displayModify,c);
		

		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 2;
		c.gridy = 0;
		supplierPanel.add(displayDelete,c);
		
		
		
		
		
		
		
		
		
		for (int i = 0; i < supplier.size(); i++) {

			JLabel newLabel = new JLabel(supplier.get(i).getName());
			newLabel.setHorizontalAlignment(SwingConstants.LEFT);

			JButton modifyButton = new JButton("Modifica Fornitore");
			modifyButton.setHorizontalAlignment(SwingConstants.LEFT);
			
			JButton deleteSupplier = new JButton("Elimina");
			deleteSupplier.setHorizontalAlignment(SwingConstants.LEFT);
			
			c.ipady=0;
			c.weightx = 0.5;
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = i+1;
			supplierPanel.add(newLabel,c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 1;
			c.gridy = i+1;
			supplierPanel.add(modifyButton,c);
			

			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 2;
			c.gridy = i+1;
			supplierPanel.add(deleteSupplier,c);
			
			

			actionEvent(deleteSupplier, supplier.get(i), modifyButton,newLabel,  this);

			validate();

		}
		
		c.fill = GridBagConstraints.HORIZONTAL;
		//c.ipady=40;
		c.weightx=0;
		c.weighty=100;
		c.gridwidth = 3;
		//c.gridheight = 20;
		c.gridx = 0;
		c.gridy = supplier.size()+1;
		supplierPanel.add(new JLabel(),c);
		validate();
	}

	@Override
	public void addObserver(SupplierController controller) {
		this.controller = controller;

		listSupplier(controller.getListSupplierView());
		
	}

	@Override
	public void setPanel(ArrayList<Supplier> supplier) {
		supplierPanel.removeAll();
		supplierPanel.setSize(10, 10);
		
		//supplierPanel.add(displayName);
		//supplierPanel.add(displayModify);
		//supplierPanel.add(displayDelete);
		
		
		
		listSupplier(supplier);
		
		validate();
	}

}
