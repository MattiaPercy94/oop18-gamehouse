package view;

import javax.swing.*;

import Utility.Utility;
import controller.SectionController;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * @author Mattia Ferrari
 */

public class ModifySectionViewImpl extends JFrame implements
		ModifySectionView {

	private static final long serialVersionUID = 1L;
	private final JPanel insertPanel = new JPanel();
	private final JLabel displayName = new JLabel("Nome :");
	private final JLabel displayMax = new JLabel("Capienza Sezione :");
	private final JLabel displayTitle = new JLabel("Modifica sezione");
	private JLabel displaynameSection;
	private final JTextField sectioName = new JTextField(20);
	private final JTextField sectioMax = new JTextField(20);
	private final JButton save = new JButton("Salva");
	private final JButton back = new JButton("Indietro");
	private JLabel labelCheck = new JLabel();
	private int sectioCode;
	SectionController controller;

	public ModifySectionViewImpl() {
		super("Modifica Sezione");
		Utility.initFrameBegin(this);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

		
//		this.setResizable(false);
//		this.setSize(700, 500);
//		this.setBounds(300, 175, this.getWidth(), this.getHeight());
//		this.setLocationRelativeTo(null);
//		Utility.centerFrame(this);
//		this.setVisible(true);
		
		SpringLayout spring = new SpringLayout();

		insertPanel.setBackground(new Color(192, 192, 192));

		this.add(insertPanel);
		insertPanel.setLayout(spring);

		insertPanel.add(displayTitle);
		displayTitle.setFont(Utility.fontTitle);
		spring.putConstraint(SpringLayout.NORTH, displayTitle, 10,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, displayTitle, 40,
				SpringLayout.WEST, this.getContentPane());
		insertPanel.add(displayName);
		displayName.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, displayName, 100,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, displayName, 40,
				SpringLayout.WEST, this.getContentPane());
		insertPanel.add(sectioName);
		sectioName.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, sectioName, 100,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, sectioName, 280,
				SpringLayout.WEST, this.getContentPane());
		insertPanel.add(displayMax);
		displayMax.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, displayMax, 180,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, displayMax, 40,
				SpringLayout.WEST, this.getContentPane());
		insertPanel.add(sectioMax);
		spring.putConstraint(SpringLayout.NORTH, sectioMax, 180,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, sectioMax, 280,
				SpringLayout.WEST, this.getContentPane());
		insertPanel.add(labelCheck);
		labelCheck.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, labelCheck, 220,
				SpringLayout.NORTH, this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, labelCheck, 280,
				SpringLayout.WEST, this.getContentPane());
		insertPanel.add(save);
		save.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, save, 250, SpringLayout.NORTH,
				this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, save, 300, SpringLayout.WEST,
				this.getContentPane());
		
		insertPanel.add(back);
		back.setFont(Utility.fontDisplay);
		spring.putConstraint(SpringLayout.NORTH, back, 250, SpringLayout.NORTH,
				this.getContentPane());
		spring.putConstraint(SpringLayout.WEST, back, 420, SpringLayout.WEST,
				this.getContentPane());

		
		back.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				controller.quitModify();
			}
		});

		save.addActionListener(new ActionListener(){
			

				@Override
				public void actionPerformed(ActionEvent arg0) {
	
					String check = null;
					try {
						check = controller.modifySection(sectioName.getText(),
								Integer.parseInt(sectioMax.getText()), sectioCode,
								displaynameSection);
		
						//labelCheck.setText(check);
						
						JOptionPane.showMessageDialog(ModifySectionViewImpl.this,check);	
						
					}catch (NumberFormatException e) {
	
						check = Utility.ERRORFORMAT;
						//labelCheck.setText(check);
						JOptionPane.showMessageDialog(ModifySectionViewImpl.this,check);
						
					}
				}
			});

		this.addWindowListener(new WindowAdapter() {

			public void windowClosing(final WindowEvent e) {

				controller.quitModify();

			}

		});
		
		Utility.initFrameEnd(this);
	}

	public void addObserver(SectionController controller) {

		this.controller = controller;
	}

	public void setData(String name, int maxQuantity, int code,
			JLabel nameSection) {

		sectioName.setText(name);
		sectioMax.setText(String.valueOf(maxQuantity));
		sectioCode = code;
		displaynameSection = nameSection;

	}

}
