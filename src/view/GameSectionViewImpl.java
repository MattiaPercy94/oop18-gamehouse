package view;

import javax.swing.*;

import Utility.Utility;
import model.Section;
import controller.GameSectionController;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;

/**
 * @author Mattia Ferrari
 */
public class GameSectionViewImpl extends JFrame implements GameSectionView {

	private static final long serialVersionUID = 1L;
	GameSectionController controller;
	//private final JPanel sectionPanel = new JPanel(	new GridLayout(0, 4, 2, 2));
	private final JPanel sectionPanel = new JPanel(new GridBagLayout());
	private final JButton backButton = new JButton("Indietro");
	private final JPanel bottomPanel = new JPanel(new GridLayout(0,3,2,2));
	private final JPanel topPanel = new JPanel(new GridBagLayout());
	
	private final JLabel displayDelete = new JLabel("Elimina",SwingConstants.LEFT);
	private final JLabel displayModify = new JLabel("Modifica",SwingConstants.LEFT);
	private final JLabel displayGame = new JLabel("Gestione Giochi",SwingConstants.LEFT);
	private final JLabel displayName = new JLabel("Nome Sezione",SwingConstants.LEFT);
	
	
	public GameSectionViewImpl() {
		super("Gestione Giochi/Sezioni");
		Utility.initFrameBegin(this);
		this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

//		this.setResizable(false);
//		this.setSize(700, 500);
//		this.setBounds(300, 175, this.getWidth(), this.getHeight());
//		this.setLocationRelativeTo(null);
//		Utility.centerFrame(this);
//		this.setVisible(true);
		
		
		this.setLayout(new BorderLayout());
		
		
		topPanel.setBackground(new Color(192, 192, 192));
		bottomPanel.setBackground(new Color(192, 192, 192));
		bottomPanel.add(new JLabel());
		bottomPanel.add(new JLabel());
		bottomPanel.add(backButton);

//		sectionPanel.add(dislayName);
//		sectionPanel.add(displayModify);
//		sectionPanel.add(displayGame);
//		sectionPanel.add(displayDelete);

		sectionPanel.setBackground(new Color(192, 192, 192));

		JScrollPane scroll = new JScrollPane(sectionPanel,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

		this.add(topPanel,BorderLayout.NORTH);
		this.add(scroll,BorderLayout.CENTER);
		this.add(bottomPanel,BorderLayout.SOUTH);

		this.addWindowListener(new WindowAdapter() {

			public void windowClosing(final WindowEvent e) {

				controller.quit();

			}

		});
		
		backButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				controller.quit();

			}

		});
		
		
		Utility.initFrameEnd(this);
	}

	public void addObserver(GameSectionController controller) {
		

		this.controller = controller;

		listSection(controller.getListSectionView());
	}

	public void listSection(ArrayList<Section> section) {
		
		GridBagConstraints c = new GridBagConstraints();
		
//		sectionPanel.add(dislayName);
//		sectionPanel.add(displayModify);
//		sectionPanel.add(displayGame);
//		sectionPanel.add(displayDelete);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 0;
		c.gridy = 0;
		sectionPanel.add(displayName,c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 1;
		c.gridy = 0;
		sectionPanel.add(displayModify,c);
		

		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 2;
		c.gridy = 0;
		sectionPanel.add(displayGame,c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.5;
		c.gridx = 3;
		c.gridy = 0;
		sectionPanel.add(displayDelete,c);

		for (int i = 0; i < section.size(); i++) {
			
			JLabel newLabel = new JLabel(section.get(i).getName());
			newLabel.setHorizontalAlignment(SwingConstants.LEFT);

			JButton modifyButton = new JButton("Modifica Sezione");
			modifyButton.setHorizontalAlignment(SwingConstants.LEFT);
			
			JButton deleteSection = new JButton("Elimina");
			deleteSection.setHorizontalAlignment(SwingConstants.LEFT);
			
			JButton directGame = new JButton("Gestione Giochi");
			directGame.setHorizontalAlignment(SwingConstants.LEFT);
			

//			JLabel newLabel = new JLabel(section.get(i).getName());
//			JButton modifyButton = new JButton("Modifica Sezione");
//			JButton deleteSection = new JButton("Elimina");
//			JButton directGame = new JButton("Gestione Giochi");
			
			c.ipady=0;
			c.weightx = 0.5;
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 0;
			c.gridy = i+1;
			sectionPanel.add(newLabel,c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 1;
			c.gridy = i+1;
			sectionPanel.add(modifyButton,c);
			

			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 2;
			c.gridy = i+1;
			sectionPanel.add(directGame,c);
			
			c.fill = GridBagConstraints.HORIZONTAL;
			c.gridx = 3;
			c.gridy = i+1;
			sectionPanel.add(deleteSection,c);

//			sectionPanel.add(newLabel);
//			sectionPanel.add(modifyButton);
//			sectionPanel.add(directGame);
//			sectionPanel.add(deleteSection);

			actionEvent(deleteSection, section.get(i), modifyButton,
					newLabel, directGame, this);

			validate();

		}
		c.fill = GridBagConstraints.HORIZONTAL;
		//c.ipady=40;
		c.weightx=0;
		c.weighty=100;
		c.gridwidth = 3;
		//c.gridheight = 20;
		c.gridx = 0;
		c.gridy = section.size()+1;
		sectionPanel.add(new JLabel(),c);
		validate();

	}

	public void actionEvent(JButton deleteSection, Section section,
			JButton modifySection, JLabel nameSection,
			JButton directGame, GameSectionViewImpl view) {

		deleteSection.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				final int exit = JOptionPane.showConfirmDialog(view,
						Utility.QUESTIONDELETE, "Cancellazione",
						JOptionPane.YES_NO_OPTION);
				if (exit == JOptionPane.YES_OPTION) {

					controller.deleteSection(section);
					if (controller.getListSectionView().size()==0) controller.quit();

				}

			}

		});

		modifySection.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				controller.setModifyController(section, nameSection);

			}

		});

		directGame.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				if ( section.getListGame().size()==0 ) {
					JOptionPane.showMessageDialog(GameSectionViewImpl.this,"Inserire prima un gioco");
				}else {
				
				controller.setModifyGameController(section);
				}
			}

		});

	}

	public void setPanel(ArrayList<Section> section) {

		sectionPanel.removeAll();
		sectionPanel.setSize(10, 10);
		
//		sectionPanel.add(dislayName);
//		sectionPanel.add(displayModify);
//		sectionPanel.add(displayGame);
//		sectionPanel.add(displayDelete);
		
		listSection(section);
		validate();

	}

}
