package view;



import controller.GameController;

/**
 * @author Mattia Ferrari
 */

public interface ModifyGame {

	/**
	 * this method use the pattern Observer
	 * 
	 * @param controller
	 */
	public void addObserver(GameController controller);

	/**
	 * this method works with the components of JFrame
	 * 
	 * @param code
	 * @param name
	 * @param price
	 * @param quantity
	 * @param viewNameGame
	 * @param viewQuantityGame
	 * @param viewPriceGame
	 */
	public void setData(int code, String name, int price, int quantity,String supplier);

}
