package view;

import javax.swing.JLabel;

import controller.SectionController;

/**
 * @author Mattia Ferrari
 */

public interface ModifySectionView {

	/**
	 * this method use the pattern Observer
	 * 
	 * @param controller
	 */
	public void addObserver(SectionController controller);

	/**
	 * this method use set the data of the GUI
	 * 
	 * @param name
	 * @param maxQuantity
	 * @param code
	 * @param nameSection
	 */
	public void setData(String name, int maxQuantity, int code, JLabel nameSection);

}
