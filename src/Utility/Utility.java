package Utility;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GraphicsEnvironment;
import java.awt.Point;

import javax.swing.JFrame;

public final class Utility {
	
	public static final String ERRORDATA = "Inserire Dati";
	public static final String ERRORCODEQUANTIY = "Codice e/o quantità max errate";
	public static final String ERRORCODE = "Codice già inserito";
	public static final String ERRORCODE2 = "Codice errato";
	public static final String ERRORNAME = "Nome già inserito";
	public static final String ERRORQUANTITY = "Quantità errata";
	public static final String ERRORCAPACITY = "Errore quantità, eccede capacità massima della sezione";
	public static final String ERRORLOGIN = "Credenziali errate";
	public static final String ERRORMODIFYQUANTIY = "Errore quantità inserita maggiore di quantità in sezione";
	public static final String ERRORFORMAT = "Elementi inseriti non giusti";
	
	public static final String CHECKQUANTITY = "Nome già inserito, ho modificato la quantità e/o prezzo";
	public static final String CHECKGAME = "Quantità e prezzo modificata";
	public static final String CHECKDELETEGAME = "Quantità reimpostata a 0, cancello il videogioco";
	
	public static final String SUCCESSMODIFY = "Modifica avvenuta con successo";
	public static final String SUCCESSINSERT = "Inserimento avvenuto con successo";
	
	
	public static final String QUESTIONLEAVE = "Stai per uscire, vuoi continuare?";
	public static final String QUESTIONDELETE = "Stai per cancellare questa sezione, vuoi continuare?";
	
	public static final String QUESTIONDELETE2 = "Stai per cancellare questo fornitore, vuoi continuare?";
	
	public static final String DELETE = "In cancellazione";
	
	
	public static final Font fontTitle = new Font(Font.SANS_SERIF,Font.PLAIN,30);
	public static final Font fontDisplayLogin = new Font(Font.SANS_SERIF,Font.PLAIN,20);
	public static final Font fontDisplay = new Font(Font.SANS_SERIF,Font.PLAIN,16);
	
	
	
	public static final void initFrameBegin(JFrame f) {
		//init grafic-----------------
		f.setState(Frame.ICONIFIED);
		f.setVisible(false);
		f.pack();
		f.setResizable(false);
		f.setSize(700, 500);
		//f.setBounds(300, 175, this.getWidth(), this.getHeight());
		f.setLocationRelativeTo(null);
		//center on screen--------------------
		Dimension windowSize = f.getSize();
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Point centerPoint = ge.getCenterPoint();

        int dx = centerPoint.x - windowSize.width / 2;
        int dy = centerPoint.y - windowSize.height / 2;    
        f.setLocation(dx, dy);
		
	}
	
	
	
	public static final void initFrameEnd(JFrame f) {
		f.setState(Frame.NORMAL);
		f.setVisible(true);
		f.validate();		
	}
	
	 public static final void centerFrame(JFrame f) {

         Dimension windowSize = f.getSize();
         GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
         Point centerPoint = ge.getCenterPoint();

         int dx = centerPoint.x - windowSize.width / 2;
         int dy = centerPoint.y - windowSize.height / 2;    
         f.setLocation(dx, dy);
	 }
}
